# Copyright (C) 2023 Collabora Limited
# Author: Ricardo Cañuelo <ricardo.canuelo@collabora.com>
#
# This module is free software; you can redistribute it and/or modify it under
# the terms of the GNU Lesser General Public License as published by the Free
# Software Foundation; either version 2.1 of the License, or (at your option)
# any later version.
#
# This library is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public License for more
# details.
#
# You should have received a copy of the GNU Lesser General Public License
# along with this library; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA

from datetime import datetime, timezone
import json

import common.legacy_kci_api as api
from common.models.legacy_kci_testcase import LegacyKCITestCase, find_test_cases
from common.models.legacy_kci_regression import LegacyKCIRegression

reg_analysis_table = 'regression_analysis'

def _find_test_history(test, before=0):
    """For a specified test run, finds in the KernelCI database all the test
    runs with its same parameters that ran after it and, optionally, a
    specified number of days before it.

    Args:
      test: LegacyKCITestCase to analyze

      before: number of days before the test run to start looking for.
        0 = consider only test runs after the specified test run.

    Returns:
      the list of test runs found
    """
    history = []
    days_before = 1
    if before > 0:
        days_before = before
    # Implementation 1: leaner and quicker, but the query is prone to timeouts
    # tests = api.find_tests(arch=test['arch'],
    #                             tree=test['job'],
    #                             branch=test['git_branch'],
    #                             defconfig=test['defconfig_full'],
    #                             device_type=test['device_type'],
    #                             test_case=test['test_case_path'],
    #                             date_range=(datetime.now() - tstamp).days + days_before)

    # Implementation 2: clunkier and slower, but higher success rate
    builds = api.find_builds(
        api_url=test.api_url,
        arch=test.arch,
        tree=test.tree,
        branch=test.branch,
        defconfig=test.defconfig,
        date_range=(datetime.now(timezone.utc) - test.date).days + days_before)
    kernels = set()
    for b in builds:
        kernels.add(b['kernel'])
    tests = []
    for k in kernels:
        t = find_test_cases(
            api_url=test.api_url,
            arch=test.arch,
            tree=test.tree,
            branch=test.branch,
            defconfig=test.defconfig,
            device_type=test.device_type,
            kernel=k,
            compiler=test.compiler,
            test_name=test.test_name,
            date_range=(datetime.now(timezone.utc) - test.date).days + days_before)
        tests.extend(t)
    # Additional results processing:
    if before == 0:
        # get only the tests that are more recent than the reference
        for t in tests:
            if t.date > test.date:
                history.append(t)
    else:
        history = tests
    # Sort test runs by date (older to newer)
    history.sort(key=lambda x: x.date)
    return history

def _find_different_defconfigs(test):
    """For a specified test run, finds in the KernelCI database all the test
    runs with its same parameters but with a different defconfig.

    Args:
      test: LegacyKCITestCase to analyze

    Returns:
      the list of tests found
    """
    tests = find_test_cases(
        api_url=test.api_url,
        arch=test.arch,
        tree=test.tree,
        branch=test.branch,
        device_type=test.device_type,
        test_name=test.test_name,
        kernel=test.kernel)
    # Additional results processing:
    # - remove the reference test
    return [t for t in tests if t.test_id != test.test_id]

def _find_different_dev_types(test):
    """For a specified test run, finds in the KernelCI database all the test
    runs with its same parameters but with a different device type.

    Args:
      test: LegacyKCITestCase to analyze

    Returns:
      the list of tests found
    """
    tests = find_test_cases(
        api_url=test.api_url,
        arch=test.arch,
        tree=test.tree,
        branch=test.branch,
        test_name=test.test_name,
        kernel=test.kernel)
    return tests

def _tests_on_same_device(test, days=1, limit=None):
    """For a specified test run, find other tests that ran on the same
    device type and the same lab since a number of days before it ran.

    This may be useful to tell whether the device in that lab was in a
    healthy status or it's something in the lab setup that was failing.

    Args:
      test: LegacyKCITestCase to use as a reference
      days: number of days before the reference test ran to search back
        from
      limit: maximum number of results to retrieve

    Returns:
      the list of tests found

    Note that this query might return a large number of results and it
    could even time out, so using a sensible <limit> is higly
    recommended
    """
    tests = find_test_cases(
        api_url=test.api_url,
        limit=limit,
        sort_field='created_on',
        sort_ascending=True,
        device_type=test.device_type,
        lab_name=test.lab,
        date_range=(datetime.now(timezone.utc) - test.date).days + days)
    return tests

def _test_kernel_results(test):
    """For a specified test run, finds in the KernelCI database all the test
    runs with the same kernel version, tree and branch.

    Args:
      test: LegacyKCITestCase to analyze

    Returns:
      the list of tests found
    """
    tests = find_test_cases(
        api_url=test.api_url,
        arch=test.arch,
        tree=test.tree,
        branch=test.branch,
        test_name=test.test_name,
        kernel=test.kernel)
    return tests

def analyze_testcase(test, days=1):
    """Performs different types of analysis on a LegacyKCITestCase.

    Args:
      - test: the LegacyKCITestCase to analyze
      - days: the number of days before the test to consider for the
        test history (1 = same day of this test run)

    Returns:

      A dict containing the lists of related tests retrieved for each
      type of analysis:
        'history': test runs since the specified date until now
        'diff_defconfigs': test runs similar to this but with different
          configs
        'diff_dev_types': test runs similar to this but with different
          device types
    """
    test_history = _find_test_history(test, before=days)
    diff_defconfigs = _find_different_defconfigs(test)
    diff_dev_types = _find_different_dev_types(test)
    tests_same_device = _tests_on_same_device(test, days=days, limit=50)
    return {'history'           : test_history,
            'diff_defconfigs'   : diff_defconfigs,
            'diff_dev_types'    : diff_dev_types,
            'tests_same_device' : tests_same_device}

def results_inspection_analysis(reg, test_runs):
    result = "failing"
    result_msg = ""
    # Rules of thumb for test result analysis:
    #
    # 1: if there's only one test result after the last failure we can't
    #    draw any conclusions from it.
    if len(test_runs) == 1 and test_runs[0].status == 'PASS':
        result = "last run passed"
        result_msg = (f"Last test run passed: ({test_runs[0].kernel}) "
                      "but there aren't enough new test results to draw a conclusion")
    else:
        passed_sequence = 0
        last_kernel_passed = ""
        state_changes = 0
        current_status = 'FAIL'
        for t in test_runs:
            if t.status != current_status:
                state_changes += 1
                current_status = t.status
            if t.status == 'PASS':
                passed_sequence += 1
                last_kernel_passed = t.kernel
            else:
                passed_sequence = 0
        # 2: if the test went from pass to fail and back to pass more
        #    than once, we consider it flaky. Exception, if a good
        #    number of runs (say, 5 or more) passed after the last fail,
        #    it could mean the test was fixed.
        if state_changes > 2:
            if passed_sequence > 4:
                result = "fixed"
            else:
                result = "flaky"
        # 3: otherwise, that means that all the test runs since the last
        #    reported fail did either pass or fail with no exceptions
        else:
            if passed_sequence > 1:
                result = "fixed"
        if result == "flaky":
            result_msg = (f"Flaky test: some runs ({last_kernel_passed}) "
                          "passed after the last failure reported "
                          f"({reg.most_recent().kernel})")
        elif result == "fixed":
            result_msg = ("The last test runs of this test passed "
                          f"(last: {last_kernel_passed}), "
                          "the issue was probably fixed")
    return result, result_msg

def analyze_regression(reg):
    """Performs different types of analysis on a LegacyKCIRegression.

    Args:
      - reg: the LegacyKCIRegression to analyze

    Returns:

      A dict containing the lists of related tests retrieved for each
      type of analysis:
        'runs_since_last_fail': test runs since the last fail until now
        'most_recent_kernel_results': all the tests runs using the most
          recent kernel version
        'first_failed_kernel_results': all the test runs using the first
          kernel version that failed
      and additional data:
        'analysis_date': timestamp of the analysis
        'result': analysis result. A single word tagging the analysis
          conclusion about the regression: "failing" / "flaky" / "fixed",
          etc.
        'result_msg': a more detailed description of the analysis conclusion
    """
    runs_since_last_fail = _find_test_history(reg.most_recent(), before=0)
    result, result_msg = results_inspection_analysis(reg, runs_since_last_fail)
    most_recent_kernel_results = _test_kernel_results(reg.most_recent())
    first_failed_kernel_results = _test_kernel_results(reg.first_failed())
    return {'analysis_date'               : datetime.now(timezone.utc),
            'runs_since_last_fail'        : runs_since_last_fail,
            'result'                      : result,
            'result_msg'                  : result_msg,
            'most_recent_kernel_results'  : most_recent_kernel_results,
            'first_failed_kernel_results' : first_failed_kernel_results
    }

def create_table(conn):
    """Create the DB table to store the regression analysis.
    Some of the fields:
     - most_recent_kernel_results
     - first_failed_kernel_results
     - runs_since_last_fail
    are compound and are encoded and stored as JSON strings. See
    store_analysis() and search_analysis() in this same module.
    """
    cur = conn.cursor()
    cur.execute(f"""CREATE TABLE IF NOT EXISTS {reg_analysis_table} (
    tree                        TEXT,
    branch                      TEXT,
    test_name                   TEXT,
    first_failed_kernel         TEXT,
    date                        TEXT,
    result                      TEXT,
    result_msg                  TEXT,
    most_recent_kernel_results  TEXT,
    first_failed_kernel_results TEXT,
    runs_since_last_fail        TEXT,
    PRIMARY KEY (tree, branch, test_name, first_failed_kernel));""")

def search_analysis(conn, regression):
    """Searches for a regression analysis in the DB. If it exists, fetch it
    and vivify all the linked objects from the stored IDs, and return
    the analysis data as a dict.

    Returns None if the analysis wasn't found in the DB.
    """
    cur = conn.cursor()
    query_str = f"""SELECT * FROM {reg_analysis_table} WHERE
    tree='{regression.tree}'           AND
    branch='{regression.branch}'       AND
    test_name='{regression.test_name}' AND
    first_failed_kernel='{regression.first_failed().kernel}'"""
    query = cur.execute(query_str).fetchall()
    if query:
        most_recent_kernel_results_ids = json.loads(query[0][7])
        most_recent_kernel_results = []
        for t_id in most_recent_kernel_results_ids:
            tests = LegacyKCITestCase.search(conn, match_params={'test_id': t_id})
            if tests:
                most_recent_kernel_results.append(tests[0])
        first_failed_kernel_results_ids = json.loads(query[0][8])
        first_failed_kernel_results = []
        for t_id in first_failed_kernel_results_ids:
            tests = LegacyKCITestCase.search(conn, match_params={'test_id': t_id})
            if tests:
                first_failed_kernel_results.append(tests[0])
        runs_since_last_fail_ids = json.loads(query[0][9])
        runs_since_last_fail = []
        for t_id in runs_since_last_fail_ids:
            tests = LegacyKCITestCase.search(conn, match_params={'test_id': t_id})
            if tests:
                runs_since_last_fail.append(tests[0])
        return {
            'analysis_date'               : query[0][4],
            'result'                      : query[0][5],
            'result_msg'                  : query[0][6],
            'most_recent_kernel_results'  : most_recent_kernel_results,
            'first_failed_kernel_results' : first_failed_kernel_results,
            'runs_since_last_fail'        : runs_since_last_fail}
    return None

def _default(obj):
    """Defines how to dump certain non-standard objects into
    JSON. Basically, define how to store dates, LegacyKCITestCases
    (store the id) and LegacyKCIRegressions (store the id)
    """
    if isinstance(obj, datetime):
        return {'_isoformat': obj.isoformat()}
    elif isinstance(obj, LegacyKCITestCase):
        return obj.test_id
    elif isinstance(obj, LegacyKCIRegression):
        return obj.regression_id
    raise TypeError(f"{type(obj)}")

def store_analysis(conn, regression):
    """Runs a regression analysis and stores it in the DB. All related tests
    found along the way are added to the DB as well.

    Returns a dict containing the analysis data as a dict, as stored in the DB.
    """
    analysis = analyze_regression(regression)
    cur = conn.cursor()
    for t in analysis['most_recent_kernel_results']:
        t.save(conn, replace=False)
    for t in analysis['first_failed_kernel_results']:
        t.save(conn, replace=False)
    for t in analysis['runs_since_last_fail']:
        t.save(conn, replace=False)
    cur.execute(f"REPLACE INTO {reg_analysis_table} VALUES(?,?,?,?,?,?,?,?,?,?)",
                (regression.tree,
                 regression.branch,
                 regression.test_name,
                 regression.first_failed().kernel,
                 analysis['analysis_date'],
                 analysis['result'],
                 analysis['result_msg'],
                 json.dumps(analysis['most_recent_kernel_results'], default=_default),
                 json.dumps(analysis['first_failed_kernel_results'], default=_default),
                 json.dumps(analysis['runs_since_last_fail'], default=_default)))
    conn.commit()
    return analysis
