# Copyright (C) 2023 Collabora Limited
# Author: Ricardo Cañuelo <ricardo.canuelo@collabora.com>
#
# This module is free software; you can redistribute it and/or modify it under
# the terms of the GNU Lesser General Public License as published by the Free
# Software Foundation; either version 2.1 of the License, or (at your option)
# any later version.
#
# This library is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public License for more
# details.
#
# You should have received a copy of the GNU Lesser General Public License
# along with this library; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA

import json
import logging
import os
import urllib

from common.utils import get_http_data, start_https_session

def _url_to_auth_token(url):
    """Returns a suitable envvar name for an authentication token based on
    an URL"""
    backend = urllib.parse.urlparse(url).netloc
    return f"TOKEN_{backend.replace('.', '_')}"
session = {}

# Implementation notes
# --------------------
#
# The main difficulty when extracting data from the legacy KernelCI API
# is to work around the limitations of the query operations. The API
# imposes certain parameter restrictions on the queries that wouldn't be
# there if we queried the database directly. Besides that, we have to
# know which parameter sets are likely to work and which ones will be
# ignored or will cause a timeout. The database is optimized to work on
# certain indexes, see
# https://github.com/kernelci/kernelci-backend/blob/main/app/handlers/dbindexes.py.
#
# In code that calls these query functions to fetch data from the API
# it's generally better to select the set of query parameters that will
# give the narrowest search results.


def _query(api_url, endpoint, params=None, retry=True):
    """Performs a configurable API query to a specified endpoint with
    optional parameters
    """
    global session
    token = os.getenv(_url_to_auth_token(api_url))
    if not token:
        raise AssertionError(f"KernelCI authentication token needed for {api_url}: \n"
                             f"set it in the {_url_to_auth_token(api_url)} environment variable.")
    if not api_url in session:
        session[api_url] = start_https_session(api_url, retry=retry)
    headers = {
        "Authorization": token
    }
    url = urllib.parse.urljoin(api_url, endpoint)
    logging.debug(f"query: {url}")
    response = get_http_data(session[api_url], url, params=params, headers=headers)
    response.raise_for_status()
    output = json.loads(response.content.decode())
    #print(json.dumps(output, indent=4))
    return output['result']

def _query_test_group(api_url, group_id=None, params=None):
    """Queries the test group endpoint"""
    if group_id:
        return _query(api_url, f"test/group/{group_id}", params)[0]
    else:
        return _query(api_url, f"test/group", params)

def query_test_case(api_url, test_case_id=None, params=None, retry=True):
    """Queries the test case endpoint"""
    if test_case_id:
        return _query(api_url, f"test/case/{test_case_id}", params=params, retry=retry)[0]
    else:
        return _query(api_url, f"test/case", params=params, retry=retry)

def query_regression(api_url, regression_id=None, params=None):
    """Queries the test regression endpoint"""
    if regression_id:
        return _query(api_url, f"test/regression/{regression_id}", params)[0]
    else:
        return _query(api_url, f"test/regression", params)

def _query_build(api_url, build_id=None, params=None):
    """Queries the build endpoint"""
    if build_id:
        return _query(api_url, f"build/{build_id}", params)[0]
    else:
        return _query(api_url, f"build", params)

def get_build(api_url, build_id):
    """Returns the params of the build identified by <build_id> from
    KernelCI
    """
    return _query_build(api_url, build_id)

def find_builds(api_url, arch=None, branch=None, build_type='kernel',
                date_range=None, defconfig=None, kernel=None,
                status='PASS',   tree=None):
    """Queries the API for kernel builds matching a set of optional
    parameters.

    All the query parameters are optional. Keep in mind that certain
    parameter combinations or lack of parameters might cause the query
    to timeout.

    Returns:
      A list of dicts containing the query results
    """
    if not any([arch, branch, defconfig, kernel, tree]):
        if not date_range or date_range < 1:
            date_range = 1
    params = {
        'arch'           : arch,
        'build_type'     : build_type,
        'date_range'     : date_range,
        'defconfig_full' : defconfig,
        'git_branch'     : branch,
        'job'            : tree,
        'kernel'         : kernel,
        'status'         : status,
    }
    return _query_build(api_url, params=params)
