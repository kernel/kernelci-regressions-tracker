# Copyright (C) 2023 Collabora Limited
# Author: Ricardo Cañuelo <ricardo.canuelo@collabora.com>
#
# This module is free software; you can redistribute it and/or modify it under
# the terms of the GNU Lesser General Public License as published by the Free
# Software Foundation; either version 2.1 of the License, or (at your option)
# any later version.
#
# This library is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public License for more
# details.
#
# You should have received a copy of the GNU Lesser General Public License
# along with this library; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA

import bisect
from collections import UserDict
from datetime import datetime, timezone

from common.models.legacy_kci_regression import LegacyKCIRegression
from ..utils import process_date_param

class Bisection():
    """Models a kernel bisection as reported by the KernelCI bot. The model
    is kept as application-independent as possible.
    """

    db_table = 'bisection'

    def __init__(self,
                 test_case=None,
                 tree=None,
                 branch=None,
                 device_type=None,
                 arch=None,
                 lab=None,
                 compiler=None,
                 defconfig=None,
                 start=None,
                 commit_title=None,
                 commit_hash=None,
                 log=None,
                 message_id=None,
                 lore_url=None,
                 report_date=None,
                 regressions=None):
        self.test_case   = test_case
        self.tree        = tree
        self.branch      = branch
        self.device_type = device_type
        self.arch        = arch
        self.lab         = lab
        self.compiler    = compiler
        self.defconfig   = defconfig
        self.start       = start
        """Commit where the bisection search started"""
        self.commit_title  = commit_title
        self.commit_hash = commit_hash
        self.log         = log
        self.lore_url    = lore_url
        """Link to the culprit patch email thread in lore.kernel.org"""
        self.report_date = process_date_param(report_date)
        self.message_id  = message_id
        """Bisection message id as it was posted in the KernelCI results mailing
        list"""
        if regressions:
            self.regressions = regressions
            """List of regressions linked to this bisection"""
            self.regressions.sort()
        else:
            self.regressions = []
        self.get_subject()

    def add_regression(self, r):
        bisect.insort(self.regressions, r)
        r.bisection_id = self.message_id

    def most_recent_regression(self):
        return self.regressions[-1]

    def get_subject(self):
        """Updates and returns the email subject based on other attributes"""
        self.subject = (f"{self.tree}/{self.branch} bisection: "
                        f"{self.test_case} on {self.device_type}")
        return self.subject

    def __str__(self):
        return (f"subject      : {self.subject}\n"
                f"test_case    : {self.test_case}\n"
                f"tree         : {self.tree}\n"
                f"branch       : {self.branch}\n"
                f"device_type  : {self.device_type}\n"
                f"commit_hash  : {self.commit_hash}\n"
                f"commit_title : {self.commit_title}\n"
                f"arch         : {self.arch}\n"
                f"lab          : {self.lab}\n"
                f"compiler     : {self.compiler}\n"
                f"defconfig    : {self.defconfig}\n"
                f"report_date  : {self.report_date}\n"
                f"start        : {self.start}\n"
                f"log          : {self.log}\n"
                f"regressions  : {self.regressions}\n"
                f"message_id   : {self.message_id}\n"
                f"lore_url     : {self.lore_url}\n")

    def __key(self):
        """A Bisection is uniquely identified by its commit hash"""
        return self.commit_hash

    def __eq__(self, other):
        if isinstance(other, Bisection):
            return self.__key() == other.__key()
        return NotImplemented

    def __hash__(self):
        return hash(self.__key())

    # ORM methods
    @classmethod
    def create_table(cls, conn):
        """Creates a DB table to store Bisection objects using an established db
        connection.

        The primary key for DB storage will be the message_id, which
        uniquely identifies a bisection _report_. Many bisection reports
        may point to the same commit hash.

        If the table exists already, this has no effect.
        """
        cur = conn.cursor()
        cur.execute(f"""CREATE TABLE IF NOT EXISTS {cls.db_table} (
        test_case TEXT,
        tree TEXT,
        branch TEXT,
        device_type TEXT,
        arch TEXT,
        lab TEXT,
        compiler TEXT,
        defconfig TEXT,
        start TEXT,
        commit_title TEXT,
        commit_hash TEXT,
        log TEXT,
        message_id TEXT PRIMARY KEY,
        lore_url TEXT,
        report_date TEXT);""")

    @classmethod
    def search(cls, conn, match_params=None, query_cond=None):
        """Searches and retrieves a list of matching Bisection objects from the
        database. The database is accessed through an established
        connection and it must contain a table for Bisection objects
        (see create_table). The search parameters are given as a
        {'parameter': 'value'} dict.
        """
        cur = conn.cursor()
        query_str = f"SELECT * FROM {cls.db_table}"
        if match_params:
            cond_list = []
            for k, v in match_params.items():
                cond_list.append(f"{k} = '{v}'")
            cond_str = ' AND '.join(cond_list)
            query_str += f" WHERE {cond_str}"
        elif query_cond:
            query_str += f" WHERE {query_cond}"
        cur.execute(query_str)
        bisections = []
        for b in cur:
            bisection = cls(*b)
            bisection.get_subject()
            regressions = LegacyKCIRegression.search(
                conn, {'bisection_id' : bisection.message_id})
            for r in regressions:
                bisection.add_regression(r)
            bisections.append(bisection)
        return bisections

    def save(self, conn):
        """Saves a Bisection object into the database using an
        established DB connection.

        The database must contain a table for Bisection objects (see
        create_table)
        """
        cur = conn.cursor()
        cur.execute(f"REPLACE INTO {self.db_table} VALUES(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)",
                    (self.test_case,    self.tree,        self.branch,
                     self.device_type,  self.arch,        self.lab,
                     self.compiler,     self.defconfig,   self.start,
                     self.commit_title, self.commit_hash, self.log,
                     self.message_id,   self.lore_url,    self.report_date,))
        conn.commit()
        for r in self.regressions:
            r.save(conn)


class Bisections(UserDict):
    """A collection (dict) of Bisection objects. If more than one Bisection
    object with the same key are added, they are collected as a list
    under that key. This allows to track multiple reports of the same
    bisection.
    """
    def __init__(self, bisections=None):
        self.data = {}
        if bisections:
            for b in bisections:
                self.data[b] = [b]

    def add(self, bisection):
        """Adds a Bisection to the collection. If the Bisection already exists,
        add it to the Bisection list under the same key.

        TODO: Check for duplicated Bisection objects: same
        report_date/test_case/etc
        """
        if bisection in self.data:
            self.data[bisection].append(bisection)
        else:
            self.data[bisection] = [bisection]

    def num_reports(self, bisection):
        """Returns the number of reports of a specific Bisection"""
        return len(self.data[bisection])

    def report_dates(self, bisection):
        """Returns the list of report dates of a Bisection

        TODO: Optional: add parameter for reverse sort
        """
        return sorted([b.report_date for b in self.data[bisection]])

    def last_report_date(self, bisection):
        """Returns the last report date of a Bisection"""
        return (self.report_dates(bisection))[-1]

    def first_report_date(self, bisection):
        """Returns the first report date of a Bisection"""
        return (self.report_dates(bisection))[0]

    def days_since_last_report(self, bisection):
        """Returns the number of days that passed since the last report of a
        Bisection until now"""
        return (datetime.now(timezone.utc) - self.last_report_date(bisection)).days

    def group_by_test(self, bisection):
        """For a specific Bisection, group all its reports by test
        (tree/branch/test_case/device_type) and return the last one
        reported
        """
        filtered = []
        bisections = []
        for b in sorted(self.data[bisection], key=lambda x: x.report_date,
                        reverse=True):
            t = {"tree": b.tree,
                 "branch": b.branch,
                 "test_case": b.test_case,
                 "device_type": b.device_type}
            if t not in filtered:
                filtered.append(t)
                bisections.append(b)
        return bisections

    def total_bisections(self):
        """Return the total number of bisection reports in the collection"""
        n = 0
        for b in self.data:
            n += len(self.data[b])
        return n

    def get_all_bisections(self):
        """Returns all the bisections as a list"""
        bisections = []
        for k, v in self.data.items():
            bisections.extend(v)
        return bisections
