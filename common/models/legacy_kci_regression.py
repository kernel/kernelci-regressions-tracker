# Copyright (C) 2023 Collabora Limited
# Author: Ricardo Cañuelo <ricardo.canuelo@collabora.com>
#
# This module is free software; you can redistribute it and/or modify it under
# the terms of the GNU Lesser General Public License as published by the Free
# Software Foundation; either version 2.1 of the License, or (at your option)
# any later version.
#
# This library is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public License for more
# details.
#
# You should have received a copy of the GNU Lesser General Public License
# along with this library; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA

import logging

import yaml
from yaml.loader import SafeLoader

from .regression import Regression, RegressionMetadata
from .legacy_kci_testcase import LegacyKCITestCase
from ..legacy_kci_api import query_regression
from ..utils import process_date_param, check_for_failures

class LegacyKCIRegressionMetadata(RegressionMetadata):
    db_table = 'legacykci_regression_metadata'
    """Name to use for the database table to store
    LegacyKCIRegressionMetadata objects
    """

    def __init__(self,
                 api_url,
                 regression_id,
                 entry_date=None,
                 status=None,
                 status_url=None,
                 regzbot_url=None,
                 extra_info=None):
        super().__init__(entry_date=process_date_param(entry_date),
                         status=status,
                         status_url=status_url,
                         regzbot_url=regzbot_url,
                         extra_info=extra_info)
        self.regression_id = regression_id
        self.api_url = api_url

    # ORM methods
    @classmethod
    def create_table(cls, conn, regression_table, regression_pkey):
        """Creates a DB table to store LegacyKCIRegressionMetadata objects using
        an established db connection. The primary key will be the
        regression_id.

        If the table exists already, this has no effect.
        """
        cur = conn.cursor()
        cur.execute(f"""CREATE TABLE IF NOT EXISTS {cls.db_table} (
        api_url       TEXT,
        regression_id TEXT,
        entry_date    TEXT,
        status        TEXT,
        status_url    TEXT,
        regzbot_url   TEXT,
        extra_info    TEXT,

        PRIMARY KEY(api_url, regression_id),
        FOREIGN KEY(api_url, regression_id) REFERENCES {regression_table}({regression_pkey}));""")

    @classmethod
    def search(cls, conn, regression_id=None):
        cur = conn.cursor()
        query_str = f"SELECT * FROM {cls.db_table}"
        if regression_id:
            query_str += f" WHERE regression_id='{regression_id}'"
        res = cur.execute(query_str)
        data = res.fetchone()
        if data:
            metadata = cls(*data)
            return metadata
        return None

    def save(self, conn):
        if not self.entry_date:
            # Don't store partial (default) metadata entries
            return
        stored = self.search(conn, regression_id=self.regression_id)
        if not stored or stored.entry_date < self.entry_date:
            # Update metadata entry
            cur = conn.cursor()
            cur.execute(f"REPLACE INTO {self.db_table} VALUES(?,?,?,?,?,?,?)",
                        (self.api_url, self.regression_id, self.entry_date, self.status,
                         self.status_url,    self.regzbot_url, self.extra_info))
            conn.commit()

    def delete(self, conn):
        cur = conn.cursor()
        cur.execute(f"DELETE FROM {self.db_table} WHERE regression_id='{self.regression_id}'")
        conn.commit()

class LegacyKCIRegression(Regression):
    """Models a regression as defined by the legacy KernelCI"""
    ADDED        = 0
    UPDATED      = 1
    FOUND        = 2
    N_SAVESTATUS = 3

    db_table = 'legacykci_regression'
    """Name to use for the database table to store LegacyKCIRegression
    objects"""

    def __init__(self,
                 api_url,
                 regression_id,
                 test_name,
                 tree,
                 branch,
                 arch,
                 device_type,
                 compiler,
                 defconfig,
                 # Date as an ISO 8601 format string or as reported by
                 # the KernelCI API (number of milliseconds since the
                 # epoch
                 date,
                 lab,
                 bisection_id=None,
                 category=None,
                 tests=None):
        super().__init__(test_name=test_name,
                         tree=tree,
                         branch=branch,
                         arch=arch,
                         device_type=device_type,
                         compiler=compiler,
                         defconfig=defconfig,
                         date=process_date_param(date),
                         lab=lab,
                         category=category,
                         tests=tests)
        self.api_url = api_url
        self.regression_id = regression_id
        self.bisection_id = bisection_id
        self.metadata = LegacyKCIRegressionMetadata(self.api_url, self.regression_id)

    def regression_link(self):
        if self.tests:
            return self.most_recent().test_link()
        return None

    def html_log(self):
        if self.tests:
            return self.most_recent().html_log()
        else:
            return "Unknown"

    def txt_log(self):
        if self.tests:
            return self.most_recent().txt_log()
        else:
            return "Unknown"

    def __str__(self):
        outstr = (f"Regression    : {self.regression_id}\n"
                  f"Backend       : {self.api_url}\n"
                  f"Plain log     : {self.txt_log()}\n"
                  f"HTML log      : {self.html_log()}\n")
        if self.bisection_id:
            outstr += f"Bisection     : {self.bisection_id}\n"
        return outstr + super().__str__() + '\n'

    def add_test(self, test):
        super().add_test(test)
        test.regression_id = self.regression_id

    @classmethod
    def from_query(cls, data):
        tests = []
        for t in data['regressions']:
            test = LegacyKCITestCase(api_url=data['api_url'],
                                     test_id=t['test_case_id']['$oid'],
                                     test_name=data['test_case_path'],
                                     tree=data['job'],
                                     branch=data['git_branch'],
                                     kernel=t['kernel'],
                                     commit=t['git_commit'],
                                     arch=data['arch'],
                                     device_type=data['device_type'],
                                     compiler=data['build_environment'],
                                     defconfig=data['defconfig_full'],
                                     date=t['created_on']['$date'],
                                     lab=t['lab_name'],
                                     status=t['status'],
                                     regression_id=data['_id']['$oid'])
            tests.append(test)
        regression = cls(api_url=data['api_url'],
                         regression_id=data['_id']['$oid'],
                         test_name=data['test_case_path'],
                         tree=data['job'],
                         branch=data['git_branch'],
                         arch=data['arch'],
                         device_type=data['device_type'],
                         compiler=data['build_environment'],
                         defconfig=data['defconfig_full'],
                         date=data['created_on']['$date'],
                         lab=data['lab_name'],
                         tests=tests)
        return regression

    @classmethod
    def from_id(cls, api_url, regression_id):
        data = query_regression(api_url, regression_id)
        data['api_url'] = api_url
        regression = cls.from_query(data)
        return regression

    # ORM methods
    @classmethod
    def create_table(cls, conn, bisection_table, bisection_pkey):
        """Creates a DB table to store LegacyKCIRegression objects using an
        established db connection. The primary key will be the
        regression_id.

        If the table exists already, this has no effect.
        """
        cur = conn.cursor()
        cur.execute(f"""CREATE TABLE IF NOT EXISTS {cls.db_table} (
        api_url       TEXT,
        regression_id TEXT,
        test_name     TEXT,
        tree          TEXT,
        branch        TEXT,
        arch          TEXT,
        device_type   TEXT,
        compiler      TEXT,
        defconfig     TEXT,
        date          TEXT,
        lab           TEXT,
        bisection_id  TEXT,
        category      TEXT,

        PRIMARY KEY(api_url, regression_id)
        FOREIGN KEY(bisection_id) REFERENCES {bisection_table}({bisection_pkey}));""")

    @classmethod
    def search(cls, conn, match_params=None, query_cond=None):
        """Searches and retrieves a list of matching LegacyKCIRegression objects
        from the database. The database is accessed through an
        established connection and it must contain a table for
        LegacyKCIRegressions (see create_table). The search parameters
        are given as a {'parameter': 'value'} dict.
        """
        cur = conn.cursor()
        query_str = f"SELECT * FROM {cls.db_table}"
        if match_params:
            cond_list = []
            for k, v in match_params.items():
                cond_list.append(f"{k} = '{v}'")
            cond_str = ' AND '.join(cond_list)
            query_str += f" WHERE {cond_str}"
        elif query_cond:
            query_str += f" WHERE {query_cond}"
        cur.execute(query_str)
        regressions = []
        for reg in cur:
            regression = cls(*reg)
            metadata = regression.metadata.search(conn, regression_id=regression.regression_id)
            if metadata:
                regression.metadata = metadata
            tests = LegacyKCITestCase.search(
                conn, {'regression_id' : regression.regression_id})
            for t in tests:
                regression.add_test(t)
            regressions.append(regression)
        return regressions

    @classmethod
    def custom_query(cls, conn, fields=None, cond=None, join=None, extra=None):
        cur = conn.cursor()
        if fields:
            fields_str = ', '.join(fields)
        else:
            fields_str = '*'
        query_str = f"SELECT {fields_str} FROM {cls.db_table}"
        if join:
            query_str += f" {join}"
        if cond:
            query_str += f" WHERE {cond}"
        if extra:
            query_str += f" {extra}"
        return cur.execute(query_str).fetchall()

    def find_regression(self, conn):
        """Searches for this regression in the DB. There are three possible
        outcomes:

        1) The exact same regression is in the DB: return it
        2) There's another version of this regression in the DB: return it
        3) No regression like this in the DB: return None
        """
        reg = self.search(conn, match_params={'regression_id' : self.regression_id})
        if reg:
            return reg[0]
        q = LegacyKCITestCase.custom_query(conn,
                                           fields=['regression_id'],
                                           cond=f"test_id='{self.last_passed().test_id}'")
        if q:
            reg = self.search(conn, match_params={'regression_id': q[0][0]})
            if reg:
                return reg[0]
        return None

    def save(self, conn):
        """Saves a LegacyKCIRegression object into the database using an
        established DB connection.

        The database must contain a table for LegacyKCIRegressions (see
        create_table)
        """
        def _store_regression(conn, obj):
            cur = conn.cursor()
            cur.execute(f"REPLACE INTO {self.db_table} VALUES(?,?,?,?,?,?,?,?,?,?,?,?,?)",
                        (obj.api_url, obj.regression_id, obj.test_name, obj.tree,
                         obj.branch,        obj.arch,         obj.device_type,
                         obj.compiler,      obj.defconfig,    obj.date,
                         obj.lab,           obj.bisection_id, obj.category))
            conn.commit()
            obj.metadata.save(conn)

        cur = conn.cursor()

        # Check for other reports of this regression. In KernelCI, every
        # new failed test run after the last one passed generates a new
        # regression object. When processing regression reports, we
        # might find a different report of an already processed
        # regression, and the difference will be that it'll contain a
        # longer or shorter sequence of failed tests since the last pass

        # Default result case
        result = self.ADDED

        reg = self.find_regression(conn)
        if reg:
            # Exact same regression is already in the DB
            if reg.regression_id == self.regression_id:
                status = self.FOUND
                logging.debug(f"Regression {self.regression_id} already in DB")
                # Update the metadata
                self.metadata.save(conn)
                # If the regression now has a different "infra" category, update it
                if self.category and self.category != reg.category:
                    reg.category = self.category
                    status =  self.UPDATED
                if self.bisection_id and not reg.bisection_id:
                    reg.bisection_id = self.bisection_id
                    status =  self.UPDATED
                if status == self.UPDATED:
                    _store_regression(conn, reg)
                return status
            else:
                # Other version of the same regression found in DB
                if len(reg.tests) > len(self.tests):
                    logging.debug(f"Newer regression {reg.regression_id} already in DB\n"
                                  f"(storing {self.regression_id})")
                    # Existing regression is newer: update it and keep
                    # it. If the currently processed regression comes
                    # from a bisection analysis, keep the existing
                    # regression and link it to the bisection report
                    if self.bisection_id and not reg.bisection_id:
                        reg.bisection_id = self.bisection_id
                        _store_regression(conn, reg)
                        return self.UPDATED
                    else:
                        return self.FOUND
                else:
                    # Existing regression is older: delete it and store
                    # new one, reuse the relevant data from the old regression:
                    # - regression category
                    # - related bisection report (if the new regression
                    #   doesn't have one already)
                    # - all related test runs
                    logging.debug(f"Older regression {reg.regression_id} already in DB\n"
                                  f"(storing {self.regression_id})")
                    self.category = reg.category
                    if not self.bisection_id:
                        self.bisection_id = reg.bisection_id
                    for t in reg.tests:
                        self.add_test(t)
                    reg.delete(conn)
                    result = self.UPDATED
        else:
            # Regression not in DB, classify it based on the test output log
            if not self.category:
                self.category, _ = check_for_failures(self.txt_log())
        _store_regression(conn, self)
        for t in self.tests:
            t.save(conn)
        return result

    def delete(self, conn):
        for t in self.tests:
            t.delete(conn)
        self.metadata.delete(conn)
        cur = conn.cursor()
        cur.execute(f"DELETE FROM {self.db_table} WHERE regression_id='{self.regression_id}'")
        conn.commit()

def update_regression_metadata(metadata, conn):
    for m in metadata['regressions']:
        stored_regs = LegacyKCIRegression.search(conn, match_params={
            'test_name'   : m['test_name'],
            'tree'        : m['tree'],
            'branch'      : m['branch']})
        for r in stored_regs:
            if (r.first_failed().kernel == m['first_fail']):
                if (not r.metadata.entry_date or
                    process_date_param(m['entry_date']) > r.metadata.entry_date):
                    r.metadata.status = m['status']
                    r.metadata.status_url = m['status_url']
                    r.metadata.regzbot_url = m['regzbot_url']
                    r.metadata.extra_info = m['extra_info']
                    r.metadata.entry_date = process_date_param(m['entry_date'])
                    r.save(conn)

def find_regressions(api_url, arch=None, branch=None, date_range=None,
                     defconfig=None, device_type=None,
                     lab_name=None,  status=None,      test_name=None,
                     tree=None,      limit=None,       sort_field=None,
                     sort_ascending=False):
    """Queries the API for regressions matching a set of optional parameters
    and returns the results as a list of LegacyKCIRegressions.

    All the query parameters are optional. If no parameters are passed,
    it retrieves all the regressions run in the past day.

    Keep in mind that certain parameter combinations or lack of parameters
    might cause the query to timeout.
    """
    if not any([arch, branch, defconfig, device_type,
                lab_name, status, test_name, tree]):
        if not date_range or date_range < 1:
            date_range = 1
    plan = None
    regressions = []
    if test_name:
        plan = test_name.split('.')[0]
    params = {
        'arch'           : arch,
        'date_range'     : date_range,
        'defconfig_full' : defconfig,
        'device_type'    : device_type,
        'git_branch'     : branch,
        'job'            : tree,
        'lab_name'       : lab_name,
        'status'         : status,
        'plan'           : plan,
        'sort'           : sort_field,
        'limit'          : limit
    }
    if sort_ascending:
        params['sort_order'] = 1
    results = query_regression(api_url, params=params)
    for r in results:
        r['api_url'] = api_url
        # Post-query filtering: if the test name was a query parameter,
        # return only the regressions matching it
        if test_name:
            if r['test_case_path'] == test_name:
                regressions.append(LegacyKCIRegression.from_query(r))
        else:
            regressions.append(LegacyKCIRegression.from_query(r))
    return regressions
