# Copyright (C) 2023 Collabora Limited
# Author: Ricardo Cañuelo <ricardo.canuelo@collabora.com>
#
# This module is free software; you can redistribute it and/or modify it under
# the terms of the GNU Lesser General Public License as published by the Free
# Software Foundation; either version 2.1 of the License, or (at your option)
# any later version.
#
# This library is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public License for more
# details.
#
# You should have received a copy of the GNU Lesser General Public License
# along with this library; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA

from datetime import datetime, timezone

import bisect

class RegressionMetadata():
    def __init__(self,
                 entry_date=None,
                 status=None,
                 status_url=None,
                 regzbot_url=None,
                 extra_info=None):
        self.entry_date = entry_date
        """Metadata entry date as a datetime object"""
        self.status = status
        self.status_url = status_url
        self.regzbot_url = regzbot_url
        self.extra_info = extra_info

    def __str__(self):
        if self.status:
            return (f"status             : {self.status}\n"
                    f"status_url         : {self.status_url}\n"
                    f"regzbot_url        : {self.regzbot_url}\n"
                    f"last status update : {self.entry_date}\n"
                    f"extra info         : {self.extra_info}\n")
        else:
            return ""

class Regression():
    """Models an application-independent regression containing the list of
    tests related to it since the last one that passed (included)"""
    def __init__(self,
                 test_name,
                 tree,
                 branch,
                 arch,
                 device_type,
                 compiler,
                 defconfig,
                 date,
                 lab,
                 category=None,
                 tests=None):
        self.test_name = test_name
        self.tree = tree
        self.branch = branch
        self.arch = arch
        self.device_type = device_type
        self.compiler = compiler
        self.defconfig = defconfig
        self.date = date
        """Regression date as a datetime object"""
        self.lab = lab
        if tests:
            self.tests = tests
            self.tests.sort()
        else:
            self.tests = []
        self.category = category
        # Metadata
        self.metadata = RegressionMetadata()

    def __eq__(self, other):
        return all([self.test_name   == other.test_name,
                    self.tree        == other.tree,
                    self.branch      == other.branch,
                    self.arch        == other.arch,
                    self.device_type == other.device_type,
                    self.compiler    == other.compiler,
                    self.defconfig   == other.defconfig,
                    self.date        == other.date,
                    self.lab         == other.lab])

    def __lt__(self, other):
        return self.date < other.date

    def __str__(self):
        outstr = (f"name          : {self.test_name}\n"
                  f"tree          : {self.tree}\n"
                  f"branch        : {self.branch}\n"
                  f"arch          : {self.arch}\n"
                  f"device_type   : {self.device_type}\n"
                  f"compiler      : {self.compiler}\n"
                  f"defconfig     : {self.defconfig}\n"
                  f"date          : {self.date}\n"
                  f"lab           : {self.lab}\n")
        if self.tests:
            outstr += f"tests         :\n"
            for t in self.tests:
                outstr += f"                {t.summary_str()}\n"
        if self.category:
            outstr += f"category      : {self.category}\n"
        outstr += f"{self.metadata}"
        return outstr

    def add_test(self, test):
        bisect.insort(self.tests, test)

    def last_passed(self):
        return self.tests[0]

    def first_failed(self):
        return self.tests[1]

    def most_recent(self):
        return self.tests[-1]

    def days_since_first_failed(self):
        return (datetime.now(timezone.utc) - self.first_failed().date).days

    def days_since_last_passed(self):
        return (datetime.now(timezone.utc) - self.last_passed().date).days

    def days_since_most_recent(self):
        return (datetime.now(timezone.utc) - self.most_recent().date).days

    def generic_id_str(self, url_safe=False):
        """Returns a string that identifies the regression generically,
        containing the tree, branch, test name and the first kernel
        version that failed. If <link_safe> is set to True, the string
        is sanitized to be used as part of a URL.
        """
        id_str = (f"{self.tree}_{self.branch}_{self.test_name}_"
                  f"{self.first_failed().kernel}")
        if url_safe:
            table = str.maketrans({'/' : '-',
                                   ':' : '-'})
            id_str = id_str.translate(table)
        return id_str
