regressions:

# A list of metadata attributes for regressions
# Each list element defines the metadata for one regression
# A regression is defined by:
#   - the test name
#   - the repo tree and branch
#   - the first kernel version that failed
# Each metadata entry must contain the date where it was last edited in
# ISO8601 format
#
# Additional info that each entry can define:
# status: a string tagging the current status of the regression: "Fixed",
#   "Reported", "False positive", etc.
# status_url: link to the relevant info for the current status, ie. an
#   email thread, message post, commit log, etc.
# regzbot_url: link to the regzbot entry if available
# extra_info: free-form text to enter additional information about the
#   regression

- test_name: "baseline.login"
  tree: "stable-rc"
  branch: "linux-4.14.y"
  first_fail: "v4.14.266-45-gce409501ca5f"
  # Metadata attributes
  status: "Reported"
  status_url: "https://lore.kernel.org/stable/1fcff522-337a-c334-42a7-bc9b4f0daec4@collabora.com/"
  regzbot_url: "https://linux-regtracking.leemhuis.info/regzbot/regression/lore/20230405132900.ci35xji3xbb3igar@rcn-XPS-13-9305/"
  entry_date: "2023-04-12T19:28:31+00:00"
  extra_info: ""

# From bisection https://groups.io/g/kernelci-results/message/42538 and
# related:
# db6f68b51e5c media: verisilicon: Do not set context src/dst formats in reset functions

- test_name: "baseline.dmesg.alert"
  tree: "mainline"
  branch: "master"
  first_fail: "v6.3-3804-g4ea956963f4f"
  status: "Reported"
  status_url: "https://lore.kernel.org/all/26addb7d-bb9d-34e8-d4fe-e323ff488101@collabora.com/"
  regzbot_url: "https://linux-regtracking.leemhuis.info/regzbot/regression/lore/26addb7d-bb9d-34e8-d4fe-e323ff488101@collabora.com/"
  entry_date: "2023-05-29T10:38:00+00:00"
  extra_info: ""

- test_name: "v4l2-compliance-uvc.device-presence"
  tree: "media"
  branch: "master"
  first_fail: "v6.3-rc2-459-g20af6be6bee4c"
  status: "Reported"
  status_url: "https://lore.kernel.org/all/26addb7d-bb9d-34e8-d4fe-e323ff488101@collabora.com/"
  regzbot_url: "https://linux-regtracking.leemhuis.info/regzbot/regression/lore/26addb7d-bb9d-34e8-d4fe-e323ff488101@collabora.com/"
  entry_date: "2023-05-29T10:38:00+00:00"
  extra_info: ""

- test_name: "baseline.dmesg.emerg"
  tree: "mainline"
  branch: "master"
  first_fail: "v6.3-5127-g5c7ecada25d2"
  status: "Reported"
  status_url: "https://lore.kernel.org/all/26addb7d-bb9d-34e8-d4fe-e323ff488101@collabora.com/"
  regzbot_url: "https://linux-regtracking.leemhuis.info/regzbot/regression/lore/26addb7d-bb9d-34e8-d4fe-e323ff488101@collabora.com/"
  entry_date: "2023-05-29T10:38:00+00:00"
  extra_info: ""

- test_name: "baseline.dmesg.emerg"
  tree: "next"
  branch: "master"
  first_fail: "next-20230411"
  status: "Reported"
  status_url: "https://lore.kernel.org/all/26addb7d-bb9d-34e8-d4fe-e323ff488101@collabora.com/"
  regzbot_url: "https://linux-regtracking.leemhuis.info/regzbot/regression/lore/26addb7d-bb9d-34e8-d4fe-e323ff488101@collabora.com/"
  entry_date: "2023-05-29T10:38:00+00:00"
  extra_info: ""

# From bisection https://groups.io/g/kernelci-results/message/42474:
# f99e6d7c4ed3 bgmac: fix *initial* chip reset to support BCM5358
- test_name: "baseline.bootrr.deferred-probe-empty"
  tree: "mainline"
  branch: "master"
  first_fail: "v6.3-rc1-2-g8ca09d5fa354"
  status: "False positive"
  status_url: "https://lore.kernel.org/all/b7b11a57-9512-cda9-1b15-5dd5aa12f162@gmail.com/"
  regzbot_url: "https://linux-regtracking.leemhuis.info/regzbot/regression/lore/20230404134613.wtikjp6v63isofoc@rcn-XPS-13-9305/"
  entry_date: "2023-06-13T09:00:00+00:00"
  extra_info: "Possible bisector misjudgement. The affected boards
  don't have a BCM5358 SoC, there's probably another unrelated driver
  that's causing the deferred probe."

# From bisection https://groups.io/g/kernelci-results/message/42476
# c20e8c5b1203 mfd: rk808: Split into core and i2c
- test_name: "baseline.bootrr.rk808-clkout-driver-present"
  tree: "next"
  branch: "master"
  first_fail: "next-20230517"
  status: "Fixed"
  status_url: "https://lore.kernel.org/all/20230518040541.299189-1-sebastian.reichel@collabora.com/"
  regzbot_url: ""
  entry_date: "2023-05-23T14:38:00+00:00"
  extra_info: "Fixed by the author before there was a regression report"

- test_name: "baseline.bootrr.dwmmc_rockchip-sdio0-probed"
  tree: "next"
  branch: "master"
  first_fail: "next-20230516"
  status: "Fixed"
  status_url: "https://lore.kernel.org/all/20230518040541.299189-1-sebastian.reichel@collabora.com/"
  regzbot_url: ""
  entry_date: "2023-06-13T09:10:00+00:00"
  extra_info: "Fixed by the author before there was a regression report"

# From bisection https://groups.io/g/kernelci-results/message/42976
# 152431e4fe7f iommu: Do iommu_group_create_direct_mappings() before attach
- test_name: "baseline.dmesg.emerg"
  tree: "next"
  branch: "master"
  first_fail: "next-20230525"
  status: "Fixed"
  status_url: "https://git.kernel.org/pub/scm/linux/kernel/git/torvalds/linux.git/commit/?id=b3fc95709c54ffbe80f16801e0a792a4d2b3d55e"
  regzbot_url: "https://linux-regtracking.leemhuis.info/regzbot/regression/lore/20230605070959.moupivvi7wr4nibu@rcn-XPS-13-9305/"
  entry_date: "2023-06-12T12:50:00+00:00"
  extra_info: ""

# From bisection https://groups.io/g/kernelci-results/message/43199
# 5ce5e0d08e34 soc: mediatek: Kconfig: Add MTK_CMDQ dependency to MTK_MMSYS
# Additional info: merged in next

- test_name: "baseline.login"
  tree: "mainline"
  branch: "master"
  first_fail: "v6.3-3804-g4ea956963f4f"
  status: "Fixed"
  status_url: "https://lore.kernel.org/dri-devel/20230518193902.891121-1-nfraprado@collabora.com/"
  regzbot_url: ""
  entry_date: "2023-06-13T09:15:00+00:00"
  extra_info: "Merged in next, not in mainline yet"

- test_name: "baseline.login"
  tree: "mainline"
  branch: "master"
  first_fail: "v6.3-1701-gd53c3eaaef6a"
  status: "Fixed"
  status_url: "https://lore.kernel.org/dri-devel/20230518193902.891121-1-nfraprado@collabora.com/"
  regzbot_url: ""
  entry_date: "2023-06-13T09:15:00+00:00"
  extra_info: "Merged in next, not in mainline yet"

- test_name: "baseline.login"
  tree: "mainline"
  branch: "master"
  first_fail: "v6.2-rc7-11-g05ecb680708a"
  status: "Fixed"
  status_url: "https://lore.kernel.org/dri-devel/20230518193902.891121-1-nfraprado@collabora.com/"
  regzbot_url: ""
  entry_date: "2023-06-13T09:15:00+00:00"
  extra_info: "Merged in next, not in mainline yet"

- test_name: "sleep.login"
  tree: "mainline"
  branch: "master"
  first_fail: "v6.3-3804-g4ea956963f4f"
  status: "Fixed"
  status_url: "https://lore.kernel.org/dri-devel/20230518193902.891121-1-nfraprado@collabora.com/"
  regzbot_url: ""
  entry_date: "2023-06-13T09:15:00+00:00"
  extra_info: "Merged in next, not in mainline yet"

- test_name: "v4l2-compliance-uvc.login"
  tree: "mainline"
  branch: "master"
  first_fail: "v6.3-3804-g4ea956963f4f"
  status: "Fixed"
  status_url: "https://lore.kernel.org/dri-devel/20230518193902.891121-1-nfraprado@collabora.com/"
  regzbot_url: ""
  entry_date: "2023-06-13T09:15:00+00:00"
  extra_info: "Merged in next, not in mainline yet"

- test_name: "baseline-nfs.login"
  tree: "mainline"
  branch: "master"
  first_fail: "v6.3-1701-gd53c3eaaef6a"
  status: "Fixed"
  status_url: "https://lore.kernel.org/dri-devel/20230518193902.891121-1-nfraprado@collabora.com/"
  regzbot_url: ""
  entry_date: "2023-06-13T09:15:00+00:00"
  extra_info: "Merged in next, not in mainline yet"

- test_name: "baseline.login"
  tree: "mainline"
  branch: "master"
  first_fail: "v6.3-1701-gd53c3eaaef6a"
  status: "Fixed"
  status_url: "https://lore.kernel.org/dri-devel/20230518193902.891121-1-nfraprado@collabora.com/"
  regzbot_url: ""
  entry_date: "2023-06-13T09:15:00+00:00"
  extra_info: "Merged in next, not in mainline yet"

- test_name: "baseline-nfs.login"
  tree: "mainline"
  branch: "master"
  first_fail: "v6.3-3804-g4ea956963f4f"
  status: "Fixed"
  status_url: "https://lore.kernel.org/dri-devel/20230518193902.891121-1-nfraprado@collabora.com/"
  regzbot_url: ""
  entry_date: "2023-06-13T09:15:00+00:00"
  extra_info: "Merged in next, not in mainline yet"

- test_name: "cros-ec.login"
  tree: "mainline"
  branch: "master"
  first_fail: "v6.3-3804-g4ea956963f4f"
  status: "Fixed"
  status_url: "https://lore.kernel.org/dri-devel/20230518193902.891121-1-nfraprado@collabora.com/"
  regzbot_url: ""
  entry_date: "2023-06-13T09:15:00+00:00"
  extra_info: "Merged in next, not in mainline yet"

- test_name: "igt-gpu-panfrost.login"
  tree: "mainline"
  branch: "master"
  first_fail: "v6.3-3804-g4ea956963f4f"
  status: "Fixed"
  status_url: "https://lore.kernel.org/dri-devel/20230518193902.891121-1-nfraprado@collabora.com/"
  regzbot_url: ""
  entry_date: "2023-06-13T09:15:00+00:00"
  extra_info: "Merged in next, not in mainline yet"

- test_name: "gt-kms-mediatek.login"
  tree: "mainline"
  branch: "master"
  first_fail: "v6.3-3804-g4ea956963f4f"
  status: "Fixed"
  status_url: "https://lore.kernel.org/dri-devel/20230518193902.891121-1-nfraprado@collabora.com/"
  regzbot_url: ""
  entry_date: "2023-06-13T09:15:00+00:00"
  extra_info: "Merged in next, not in mainline yet"

- test_name: "kselftest-alsa.login"
  tree: "mainline"
  branch: "master"
  first_fail: "v6.3-3804-g4ea956963f4f"
  status: "Fixed"
  status_url: "https://lore.kernel.org/dri-devel/20230518193902.891121-1-nfraprado@collabora.com/"
  regzbot_url: ""
  entry_date: "2023-06-13T09:15:00+00:00"
  extra_info: "Merged in next, not in mainline yet"

- test_name: "kselftest-arm64.login"
  tree: "mainline"
  branch: "master"
  first_fail: "v6.3-3804-g4ea956963f4f"
  status: "Fixed"
  status_url: "https://lore.kernel.org/dri-devel/20230518193902.891121-1-nfraprado@collabora.com/"
  regzbot_url: ""
  entry_date: "2023-06-13T09:15:00+00:00"
  extra_info: "Merged in next, not in mainline yet"

- test_name: "kselftest-rtc.login"
  tree: "mainline"
  branch: "master"
  first_fail: "v6.3-3804-g4ea956963f4f"
  status: "Fixed"
  status_url: "https://lore.kernel.org/dri-devel/20230518193902.891121-1-nfraprado@collabora.com/"
  regzbot_url: ""
  entry_date: "2023-06-13T09:15:00+00:00"
  extra_info: "Merged in next, not in mainline yet"

- test_name: "kselftest-tpm2.login"
  tree: "mainline"
  branch: "master"
  first_fail: "v6.3-3804-g4ea956963f4f"
  status: "Fixed"
  status_url: "https://lore.kernel.org/dri-devel/20230518193902.891121-1-nfraprado@collabora.com/"
  regzbot_url: ""
  entry_date: "2023-06-13T09:15:00+00:00"
  extra_info: "Merged in next, not in mainline yet"

- test_name: "lc-compliance.login"
  tree: "mainline"
  branch: "master"
  first_fail: "v6.3-3804-g4ea956963f4f"
  status: "Fixed"
  status_url: "https://lore.kernel.org/dri-devel/20230518193902.891121-1-nfraprado@collabora.com/"
  regzbot_url: ""
  entry_date: "2023-06-13T09:15:00+00:00"
  extra_info: "Merged in next, not in mainline yet"

- test_name: "v4l2-compliance-uvc.login"
  tree: "media"
  branch: "master"
  first_fail: "v6.4-rc2-26-ga23a3041c733e"
  status: "Fixed"
  status_url: "https://lore.kernel.org/dri-devel/20230518193902.891121-1-nfraprado@collabora.com/"
  regzbot_url: ""
  entry_date: "2023-06-13T09:15:00+00:00"
  extra_info: "Merged in next, not in mainline yet"

- test_name: "baseline.bootrr.rk808-clkout-driver-present"
  tree: "mainline"
  branch: "master"
  first_fail: "v6.4-4247-g3a8a670eeeaa"
  status: "Fixed"
  status_url: "https://git.kernel.org/pub/scm/linux/kernel/git/torvalds/linux.git/commit/?id=ffd791349859c47c50e1e423295b4f8912c45ee0"
  regzbot_url: ""
  entry_date: "2023-07-04T11:15:00+00:00"
  extra_info: "Fix already in mainline, KernelCI tests need to catch up"

- test_name: "baseline.bootrr.deferred-probe-empty"
  tree: "mainline"
  branch: "master"
  first_fail: "v6.3-rc4-25-g3a93e40326c8"
  status: "False positive"
  status_url: ""
  regzbot_url: ""
  entry_date: "2023-07-04T14:10:00+00:00"
  extra_info: "KernelCI kernel/modules builds for this test lack certain soundcard-related drivers and codecs. Fixing the kernel config would fix this test"

- test_name: "baseline-nfs.bootrr.deferred-probe-empty"
  tree: "mainline"
  branch: "master"
  first_fail: "v6.3-rc4-25-g3a93e40326c8"
  status: "False positive"
  status_url: ""
  regzbot_url: ""
  entry_date: "2023-07-04T14:10:00+00:00"
  extra_info: "KernelCI kernel/modules builds for this test lack certain soundcard-related drivers and codecs. Fixing the kernel config would fix this test"

- test_name: "baseline-nfs.bootrr.deferred-probe-empty"
  tree: "next"
  branch: "master"
  first_fail: "next-20230328"
  status: "False positive"
  status_url: ""
  regzbot_url: ""
  entry_date: "2023-07-04T14:10:00+00:00"
  extra_info: "KernelCI kernel/modules builds for this test lack certain soundcard-related drivers and codecs. Fixing the kernel config would fix this test"

- test_name: "baseline.bootrr.deferred-probe-empty"
  tree: "next"
  branch: "master"
  first_fail: "next-20230328"
  status: "False positive"
  status_url: ""
  regzbot_url: ""
  entry_date: "2023-07-04T14:10:00+00:00"
  extra_info: "KernelCI kernel/modules builds for this test lack certain soundcard-related drivers and codecs. Fixing the kernel config would fix this test"

- test_name: "v4l2-decoder-conformance-vp8.login"
  tree: "mainline"
  branch: "master"
  first_fail: "v6.5-rc3-275-gffabf7c731765"
  status: "False positive"
  status_url: ""
  regzbot_url: ""
  entry_date: "2023-08-04T10:55:00+00:00"
  extra_info: "Infrastructure error due to serial connection unreliability. Test completed after retrying."

- test_name: "v4l2-decoder-conformance-vp9.login"
  tree: "mainline"
  branch: "master"
  first_fail: "v6.5-rc4-161-g999f6631866e9"
  status: "False positive"
  status_url: ""
  regzbot_url: ""
  entry_date: "2023-08-04T10:55:00+00:00"
  extra_info: "Infrastructure error due to serial connection unreliability. Test completed after retrying."

- test_name: "v4l2-decoder-conformance-h264.login"
  tree: "next"
  branch: "master"
  first_fail: "next-20230802"
  status: "Fixed"
  status_url: ""
  regzbot_url: ""
  entry_date: "2023-08-04T10:55:00+00:00"
  extra_info: "Fix was sent by the author: https://lore.kernel.org/lkml/2023080224-nutrient-regress-c95e@gregkh/T/"

- test_name: "v4l2-decoder-conformance-h265.login"
  tree: "next"
  branch: "master"
  first_fail: "next-20230801"
  status: "Fixed"
  status_url: ""
  regzbot_url: ""
  entry_date: "2023-08-04T10:55:00+00:00"
  extra_info: "Fix was sent by the author: https://lore.kernel.org/lkml/2023080224-nutrient-regress-c95e@gregkh/T/"

- test_name: "v4l2-decoder-conformance-h265.login"
  tree: "next"
  branch: "master"
  first_fail: "next-20230802"
  status: "Fixed"
  status_url: ""
  regzbot_url: ""
  entry_date: "2023-08-04T10:55:00+00:00"
  extra_info: "Fix was sent by the author: https://lore.kernel.org/lkml/2023080224-nutrient-regress-c95e@gregkh/T/"

- test_name: "v4l2-decoder-conformance-vp8.login"
  tree: "next"
  branch: "master"
  first_fail: "next-20230802"
  status: "Fixed"
  status_url: ""
  regzbot_url: ""
  entry_date: "2023-08-04T10:55:00+00:00"
  extra_info: "Fix was sent by the author: https://lore.kernel.org/lkml/2023080224-nutrient-regress-c95e@gregkh/T/"

- test_name: "v4l2-decoder-conformance-vp8.login"
  tree: "next"
  branch: "master"
  first_fail: "next-20230803"
  status: "Fixed"
  status_url: ""
  regzbot_url: ""
  entry_date: "2023-08-04T10:55:00+00:00"
  extra_info: "Fix was sent by the author: https://lore.kernel.org/lkml/2023080224-nutrient-regress-c95e@gregkh/T/"

- test_name: "v4l2-decoder-conformance-vp9.login"
  tree: "next"
  branch: "master"
  first_fail: "next-20230802"
  status: "Fixed"
  status_url: ""
  regzbot_url: ""
  entry_date: "2023-08-04T10:55:00+00:00"
  extra_info: "Fix was sent by the author: https://lore.kernel.org/lkml/2023080224-nutrient-regress-c95e@gregkh/T/"

- test_name: "cros-tast-mm-decode-v4l2-stateful-vp9.login"
  tree: "kernelci"
  branch: "chromeos-stable"
  first_fail: "chromeos-stable-20230802.0"
  status: "False positive"
  status_url: ""
  regzbot_url: ""
  entry_date: "2023-08-04T10:55:00+00:00"
  extra_info: "Infrastucture error, depthcharge failed to boot"

- test_name: "cros-tast-mm-decode-v4l2-stateful-vp9.login"
  tree: "kernelci"
  branch: "chromeos-stable"
  first_fail: "chromeos-stable-20230802.3"
  status: "False positive"
  status_url: ""
  regzbot_url: ""
  entry_date: "2023-08-04T10:55:00+00:00"
  extra_info: "Infrastructure error due to serial connection unreliability."