Template for reporting regression

Hi Martin,

KernelCI found this patch causes a regression in the baseline.login test on rk3328-rock64, 
see the following details for more information.

KernelCI dashboard link: 
https://linux.kernelci.org/test/case/id/617f11f5c157b666fb3358e6/

Error messages from the logs:

[    0.033465] CPU: CPUs started in inconsistent modes
[    0.033557] Unexpected kernel BRK exception at EL1
[    0.034432] Internal error: BRK handler: f2000800 [#1] PREEMPT SMP

Test case failing:
Baseline Login Test - Link-to-the-test-case-code

[Optional]
Regression Reproduced:

* Link to the Lava Jobs if any.
* Describe the steps of how it was reproduced.
* Attach config file, kernel image, rootfs, etc, if necessary.

Bisection report from KernelCI can be found at the bottom of the email.

Thanks,
Shreeya Patel

#regzbot introduced: 675c496d0f92
#regzbot title: KernelCI: rk3328-rock64 stopped booting 

-----------------------------------------------------------------------------------------------------------------------------------------------------

Bisection report from KernelCI:
https://groups.io/g/kernelci-results/message/18553

*	If you do send a fix, please include this trailer:	*
*	Reported-by: "kernelci.org bot" <bot@kernelci.org>	*

mainline/master bisection: baseline.login on rk3328-rock64

Summary:
  Start:      3a4347d82efdf Merge tag 'clk-fixes-for-linus' of git://git.kernel.org/pub/scm/linux/kernel/git/clk/linux
  Plain log:  https://storage.kernelci.org/mainline/master/v5.15-rc7-214-g3a4347d82efd/arm64/defconfig/gcc-10/lab-baylibre/baseline-rk3328-rock64.txt
  HTML log:   https://storage.kernelci.org/mainline/master/v5.15-rc7-214-g3a4347d82efd/arm64/defconfig/gcc-10/lab-baylibre/baseline-rk3328-rock64.html
  Result:     675c496d0f92b clk: composite: Also consider .determine_rate for rate + mux composites

Checks:
  revert:     PASS
  verify:     PASS

Parameters:
  Tree:       mainline
  URL:        https://git.kernel.org/pub/scm/linux/kernel/git/torvalds/linux.git
  Branch:     master
  Target:     rk3328-rock64
  CPU arch:   arm64
  Lab:        lab-baylibre
  Compiler:   gcc-10
  Config:     defconfig
  Test case:  baseline.login

Breaking commit found:

-------------------------------------------------------------------------------
commit 675c496d0f92b481ebe4abf4fb06eadad7789de6
Author: Martin Blumenstingl <martin.blumenstingl@...>
Date:   Sat Oct 16 12:50:21 2021 +0200

    clk: composite: Also consider .determine_rate for rate + mux composites

    Commit 69a00fb3d69706 ("clk: divider: Implement and wire up
    .determine_rate by default") switches clk_divider_ops to implement
    .determine_rate by default. This breaks composite clocks with multiple
    parents because clk-composite.c does not use the special handling for
    mux + divider combinations anymore (that was restricted to rate clocks
    which only implement .round_rate, but not .determine_rate).
    
    Alex reports:
      This breaks lot of clocks for Rockchip which intensively uses
      composites,  i.e. those clocks will always stay at the initial parent,
      which in some cases  is the XTAL clock and I strongly guess it is the
      same for other platforms,  which use composite clocks having more than
      one parent (e.g. mediatek, ti ...)
    
      Example (RK3399)
      clk_sdio is set (initialized) with XTAL (24 MHz) as parent in u-boot.
      It will always stay at this parent, even if the mmc driver sets a rate
      of  200 MHz (fails, as the nature of things), which should switch it
      to   any of its possible parent PLLs defined in
      mux_pll_src_cpll_gpll_npll_ppll_upll_24m_p (see clk-rk3399.c)  - which
      never happens.
    
    Restore the original behavior by changing the priority of the conditions
    inside clk-composite.c. Now the special rate + mux case (with rate_ops
    having a .round_rate - which is still the case for the default
    clk_divider_ops) is preferred over rate_ops which have .determine_rate
    defined (and not further considering the mux).
    
    Fixes: 69a00fb3d69706 ("clk: divider: Implement and wire up .determine_rate by default")
    Reported-by: Alex Bee <knaerzche@...>
    Signed-off-by: Martin Blumenstingl <martin.blumenstingl@...>
    Link: https://lore.kernel.org/r/20211016105022.303413-2-martin.blumenstingl@googlemail.com
    Tested-by: Alex Bee <knaerzche@...>
    Tested-by: Chen-Yu Tsai <wens@...>
    Signed-off-by: Stephen Boyd <sboyd@...>

diff --git a/drivers/clk/clk-composite.c b/drivers/clk/clk-composite.c
index 0506046a5f4b9..510a9965633bb 100644
--- a/drivers/clk/clk-composite.c
+++ b/drivers/clk/clk-composite.c
@@ -58,11 +58,8 @@ static int clk_composite_determine_rate(struct clk_hw *hw,
         long rate;
         int i;
 
-        if (rate_hw && rate_ops && rate_ops->determine_rate) {
-                __clk_hw_set_clk(rate_hw, hw);
-                return rate_ops->determine_rate(rate_hw, req);
-        } else if (rate_hw && rate_ops && rate_ops->round_rate &&
-                   mux_hw && mux_ops && mux_ops->set_parent) {
+        if (rate_hw && rate_ops && rate_ops->round_rate &&
+            mux_hw && mux_ops && mux_ops->set_parent) {
                 req->best_parent_hw = NULL;
 
                 if (clk_hw_get_flags(hw) & CLK_SET_RATE_NO_REPARENT) {
@@ -107,6 +104,9 @@ static int clk_composite_determine_rate(struct clk_hw *hw,
 
                 req->rate = best_rate;
                 return 0;
+        } else if (rate_hw && rate_ops && rate_ops->determine_rate) {
+                __clk_hw_set_clk(rate_hw, hw);
+                return rate_ops->determine_rate(rate_hw, req);
         } else if (mux_hw && mux_ops && mux_ops->determine_rate) {
                 __clk_hw_set_clk(mux_hw, hw);
                 return mux_ops->determine_rate(mux_hw, req);
-------------------------------------------------------------------------------


Git bisection log:

-------------------------------------------------------------------------------
git bisect start
# good: [119c85055d867b9588263bca59794c872ef2a30e] Merge tag 'powerpc-5.15-6' of git://git.kernel.org/pub/scm/linux/kernel/git/powerpc/linux
git bisect good 119c85055d867b9588263bca59794c872ef2a30e
# bad: [3a4347d82efdfcc5465b3ed37616426989182915] Merge tag 'clk-fixes-for-linus' of git://git.kernel.org/pub/scm/linux/kernel/git/clk/linux
git bisect bad 3a4347d82efdfcc5465b3ed37616426989182915
# good: [54c5639d8f507ebefa814f574cb6f763033a72a5] riscv: Fix asan-stack clang build
git bisect good 54c5639d8f507ebefa814f574cb6f763033a72a5
# bad: [675c496d0f92b481ebe4abf4fb06eadad7789de6] clk: composite: Also consider .determine_rate for rate + mux composites
git bisect bad 675c496d0f92b481ebe4abf4fb06eadad7789de6
# first bad commit: [675c496d0f92b481ebe4abf4fb06eadad7789de6] clk: composite: Also consider .determine_rate for rate + mux composites
-------------------------------------------------------------------------------

Github: Link-to-the-github-issue
