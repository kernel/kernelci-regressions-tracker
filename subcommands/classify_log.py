#!/usr/bin/env python3
#
# Copyright (C) 2023 Collabora Limited
# Author: Gustavo Padovan <gustavo.padovan@collabora.com>
#
# This module is free software; you can redistribute it and/or modify it under
# the terms of the GNU Lesser General Public License as published by the Free
# Software Foundation; either version 2.1 of the License, or (at your option)
# any later version.
#
# This library is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public License for more
# details.
#
# You should have received a copy of the GNU Lesser General Public License
# along with this library; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA

from common.utils import classify_failure

import requests

def run_classify_failure(args):
    if "https" in args.file:
        response = requests.get(args.file)
        log = response.content.decode("utf-8")
    else:
        log = open(args.file).read()

    type, lines = classify_failure(log)

    print("Failure type: {}".format(type))
    print("Log lines identified:")
    for l in lines:
        print("- {}".format(l))

def rt_add_classify_log_cmd(subparsers):
    parser = subparsers.add_parser('classify-log',
                help='classify type of log in the kernel log, looking for infra failures')
    parser.add_argument('-f', '--file', type=str, required=True,
                      help="the local log file or link to log file")
    parser.set_defaults(func=run_classify_failure)


