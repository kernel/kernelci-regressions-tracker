#!/usr/bin/env python3
#
# Copyright (C) 2023 Collabora Limited
# Author: Gustavo Padovan <gustavo.padovan@collabora.com>
# Author: Ricardo Cañuelo <ricardo.canuelo@collabora.com>
#
# This module is free software; you can redistribute it and/or modify it under
# the terms of the GNU Lesser General Public License as published by the Free
# Software Foundation; either version 2.1 of the License, or (at your option)
# any later version.
#
# This library is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public License for more
# details.
#
# You should have received a copy of the GNU Lesser General Public License
# along with this library; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA

from datetime import datetime, timezone, timedelta
from http.cookiejar import MozillaCookieJar
import json
import logging
import os
from pathlib import Path
import pickle
import re
import requests
import sys
import traceback
import urllib

import html2text
import threading
import yaml
from yaml.loader import SafeLoader

import common.db_helpers as db_helpers
from common.db_helpers import db_setup, db_conn
from common.models.bisection import Bisection, Bisections
from common.models.legacy_kci_regression import LegacyKCIRegression, find_regressions, update_regression_metadata
from common.models.legacy_kci_testcase import LegacyKCITestCase, find_test_cases
import common.legacy_kci_api as api
from common.lore  import get_lore_url
from common.utils import get_http_data, start_https_session, check_for_failures
from common.html_views import generate_html_reports
import common.analysis


GROUPSIO_SERVER = "https://groups.io"
REGZBOT_SRV="https://linux-regtracking.leemhuis.info/regzbot/"

def _load_cookie():
    jar = MozillaCookieJar(Path('{}/cookies'.format(sys.path[0])))
    jar.load()
    return jar

def fetch_messages(n_msg, load_from=None, dump_to=None):
    """Get messages from the groups.io archive

        API doc: https://groups.io/api#get_messages

        Args:
          n_msg: the number of messages to fetch
          load_from: file name to load messages from

        Returns:
          A list of emails ordered by most recent first
    """

    if load_from:
        with open(load_from, 'rb') as handle:
            emails = pickle.load(handle)
        logging.info(f"{len(emails)} emails loaded from pickle dump.")
        return emails

    jar = _load_cookie()
    url = urllib.parse.urljoin(GROUPSIO_SERVER, "api/v1/getmessages")
    params = {
        'group_name': 'kernelci-results',
        'limit':      '100',
        'sort_dir':   'desc'    # Most recent first
    }

    session = start_https_session(GROUPSIO_SERVER, cookie_jar=jar)

    emails = []
    has_more = True
    while (has_more and n_msg == 0) or n_msg > len(emails):
        try:
            response = get_http_data(session, url, params)
        except:
            logging.error(f"Failed to fetch messages at {url} with params = {params}.")
            raise

        data = json.loads(response.content.decode())

        emails += data['data']
        params['page_token'] = data['next_page_token']
        has_more = data['has_more']

    if dump_to:
        with open(dump_to, 'wb') as handle:
            pickle.dump(emails, handle, protocol=pickle.HIGHEST_PROTOCOL)

        logging.info(f"Pickle dump of the messages saved at '{dump_to}'.")
        exit(0)

    logging.info(f"{len(emails)} emails fetched.")
    return emails

def parse_date(date):
    # Which time format is groupio using??
    # Not exactly ISO 8601 as understood by python's datetime:
    #     eg. 2023-02-20T10:10:42.554744814-08:00
    # We have to either drop the microseconds or redefine the timezone.
    # Dropping the microseconds for now:
    m = re.match('(.+)\.\d+(.+)', date)
    return datetime.fromisoformat(m.group(1) + m.group(2)) if m else None

def _email_to_txt(body):
    text = html2text.HTML2Text()
    text.ignore_links = True
    text.body_width = 300
    return text.handle(body)

def get_bisection_info(msg, msg_id, created, branches):
    patterns = {
        'tree'           : '^Tree: ([^\s]+)',
        'branch'         : '^Branch: ([^\s]+)',
        'device_type'    : '^Target: ([^\s]+)',
        'arch'           : '^CPU arch: ([^\s]+)',
        'lab'            : '^Lab: ([^\s]+)',
        'compiler'       : '^Compiler: ([^\s]+)',
        'defconfig'      : '^Config: ([^\s]+)',
        'test_case'      : '^Test case: ([^\s]+)',
        'start'          : '^Start: (.+)  \n',
        'commit_title'   : '^Result: [0-9a-f]+ (.+)  \n',
        'commit_hash'    : '^# first bad commit: \[(\w+)\]',
        'log'            : '^Plain log: (.+)  \n',
    }
    # Search for bisection in database
    match = Bisection.search(db_conn(), {'message_id' : msg_id})
    if match:
        bisection = match[0]
    else:
        # New bisection: parse it and create a Bisection object
        bisection = Bisection(message_id=msg_id)
        for param, pattern in patterns.items():
            m = re.search(pattern, msg, flags=re.MULTILINE)
            if m:
                setattr(bisection, param, m.group(1))
            else:
                logging.error(f"Error while parsing bisection info: (pattern: {pattern})")
                raise AssertionError
        bisection.get_subject()
        bisection.report_date = created
    logging.debug(f"Bisection {bisection.message_id}")

    # Find related regressions and link to them (only for bisections
    # affecting the tracked trees/branches)
    tree_branch_api = {}
    for b in branches:
        tree_branch_api[f"{b['tree']}/{b['branch']}"] = b['api_url']
    if f"{bisection.tree}/{bisection.branch}" not in tree_branch_api:
        logging.debug("Bisection not in any of the tracked trees/branches. "
                      "Don't fetch related test info")
        return bisection
    m = re.search(f'{bisection.tree}/{bisection.branch}/([^/]+)/', bisection.log)
    if m:
        kernel_ver = m.group(1)
    logging.debug(f"Find related tests: \n"
                  f"arch:        {bisection.arch}\n"
                  f"tree:        {bisection.tree}\n"
                  f"branch:      {bisection.branch}\n"
                  f"test_name:   {bisection.test_case}\n"
                  f"defconfig:   {bisection.defconfig}\n"
                  f"device_type: {bisection.device_type}\n"
                  f"kernel:      {kernel_ver}\n"
                  f"lab_name:    {bisection.lab}")
    related_tests = find_test_cases(api_url=tree_branch_api[f"{bisection.tree}/{bisection.branch}"],
                                    arch=bisection.arch,
                                    tree=bisection.tree,
                                    branch=bisection.branch,
                                    test_name=bisection.test_case,
                                    defconfig=bisection.defconfig,
                                    device_type=bisection.device_type,
                                    kernel=kernel_ver,
                                    lab_name=bisection.lab,
                                    status='FAIL')
    logging.debug(f"Related tests found: {len(related_tests)}")
    for t in related_tests:
        if t.regression_id:
            match = LegacyKCIRegression.search(db_conn(),
                                               {'regression_id' : t.regression_id})
            if match:
                regression = match[0]
                logging.debug(f"Matching regression in DB: {regression.regression_id}")
                bisection.add_regression(regression)
            else:
                logging.debug(f"No matching regression in DB: {t.regression_id}. "
                              "Get it from KernelCI")
                regression = LegacyKCIRegression.from_id(tree_branch_api[f"{bisection.tree}/{bisection.branch}"],
                                                         t.regression_id)
                if regression:
                    logging.debug(f"Regression found and added to bisection")
                    bisection.add_regression(regression)
    return bisection

def get_lore_for_bisections(bisections):
    for bisection, bisection_list in bisections.items():
        if not bisection.lore_url:
            bisection.lore_url = get_lore_url(bisection.commit_title)
            if not bisection.lore_url:
                bisection.lore_url = 'unknown'
            for b in bisection_list:
                b.lore_url = bisection.lore_url

def collect_bisections(emails, branches):
    bisections = Bisections()
    for msg in emails:
        if msg['name'] == 'KernelCI bot' and "bisection" in msg['subject']:
            bisection_date = parse_date(msg['created'])
            if not bisection_date:
                logging.error(f"Failed to parse bisection info for msg_id {msg['msg_num']}")
                continue
            try:
                bisection = get_bisection_info(_email_to_txt(msg['body']),
                                               msg['msg_num'], bisection_date,
                                               branches)
            except AssertionError as e:
                logging.exception(f"Unexpected error during bisection processing: {e}")
                continue
            bisections.add(bisection)
    logging.info(f"{bisections.total_bisections()} bisection emails total, "
                 f"grouped as {len(bisections)} distinct bisections.")
    return bisections

def collect_regressions(branches, days=1):
    """Gets regression info from the KernelCI database for the repo
    trees/branches specified in <branches>, saves each regression as a
    LegacyKCIRegression object in the tracker database.
    """
    for branch in branches:
        logging.info(f"Collecting regressions for {branch['tree']}/{branch['branch']}")
        # Original method: query regressions. Disable it temporarily
        # because these requests are now (Aug 22 2023) timing out.
        # regressions = find_regressions(api_url=branch['api_url'],
        #                                tree=branch['tree'],
        #                                branch=branch['branch'],
        #                                date_range=days)

        # Alternative method: query test cases (less prone to request
        # timeouts) and scan them to search for regressions
        tests = find_test_cases(api_url=branch['api_url'],
                                tree=branch['tree'],
                                branch=branch['branch'],
                                date_range=days)
        regression_ids = []
        for t in tests:
            if t.regression_id:
                regression_ids.append(t.regression_id)
        regressions = []
        for reg_id in regression_ids:
            r = LegacyKCIRegression.from_id(branch['api_url'], reg_id)
            regressions.append(r)
        logging.info(f"{len(regressions)} regressions found")
        results = [0] * LegacyKCIRegression.N_SAVESTATUS
        for r in regressions:
            results[r.save(db_conn())] += 1
        logging.info(f"Already in DB: {results[LegacyKCIRegression.FOUND]}, "
                     f"updated: {results[LegacyKCIRegression.UPDATED]}, "
                     f"new added: {results[LegacyKCIRegression.ADDED]}")

def regression_analysis(date_threshold=None):
    """Runs, saves and returns a regression analysis for all the regressions
    in the DB.

    For each regression group, searches for an analysis of it already in
    the DB. If there's no analysis for it, run it and store it. If
    there's an analysis already, return it.

    Return a dict containing the full analysis of each regression group
    as defined by analysis.analyze_regression() well as the list of
    regressions in each regression group.
    """
    regressions = LegacyKCIRegression.search(db_conn())
    common.analysis.create_table(db_conn())
    analysis = {}
    if date_threshold != None:
        threshold = datetime.now(timezone.utc) - timedelta(days=date_threshold)
    for r in regressions:
        reg_analysis = common.analysis.search_analysis(db_conn(), r)
        if (not reg_analysis or
            (date_threshold != None and
             datetime.fromisoformat(reg_analysis['analysis_date']) < threshold)):
            if not reg_analysis:
                logging.debug("No analysis found in DB for regression "
                              f"{r.regression_id}: run analysis")
            else:
                logging.debug(f"Analysis for regression {r.regression_id} "
                              f"({reg_analysis['analysis_date']}) "
                              f"is older than {threshold}: re-run analysis")
            try:
                reg_analysis = common.analysis.store_analysis(db_conn(), r)
                analysis[r.generic_id_str()] = reg_analysis
                analysis[r.generic_id_str()]['regressions'] = [r]
            except Exception as e:
                logging.error("Error analyzing regression "
                              f"{r.generic_id_str()}: "
                              "skipping analysis")
                logging.error(traceback.format_exc())
            # Update the regression log analysis as well
            r.category, _ = check_for_failures(r.txt_log())
            r.save(db_conn())
        else:
            logging.debug(f"Analysis found in DB for regression {r.regression_id}")
            if r.generic_id_str() in analysis:
                if 'regressions' in analysis[r.generic_id_str()]:
                    analysis[r.generic_id_str()]['regressions'].append(r)
                else:
                    analysis[r.generic_id_str()]['regressions'] = [r]
            else:
                analysis[r.generic_id_str()] = reg_analysis
                analysis[r.generic_id_str()]['regressions'] = [r]
    return analysis


def compile_reporting_status(metadata):
    session = start_https_session(REGZBOT_SRV)
    regzbot_url = urllib.parse.urljoin(REGZBOT_SRV, "regressions.json")

    try:
        resp = get_http_data(session, regzbot_url)
    except requests.exceptions.HTTPError as err:
        logging.error(f"Failed to get Regzbot data: {err}.")
        return None

    regz_regressions = {}
    for r in json.loads(resp.text):
        regz_regressions[r['url_regzbot']] = r

    status = []
    for m in metadata['regressions']:
        # Add regzbot data if this metadata entry links to a regzbot
        # report
        if m['regzbot_url'] in regz_regressions.keys():
            regz = regz_regressions[m['regzbot_url']]
            m['regz'] = regz
            m['status'] = regz['solved']['reason']
        # Search the regression in the DB to generate a link to its
        # analysis page
        matches = LegacyKCIRegression.search(db_conn(),
                                             {'test_name' : m['test_name'],
                                              'tree'      : m['tree'],
                                              'branch'    : m['branch']})
        for r in matches:
            if r.first_failed().kernel == m['first_fail']:
                m['analysis_page'] = r.generic_id_str(url_safe=True) + ".html"
                break
        status.append(m)
    return status

def run_tracker(args):
    if args.branches:
        config_file = args.branches
    else:
        config_file = os.path.join("configs", "tracked-branches.yaml")
    with open(config_file) as f:
        branches = yaml.load(f, Loader=SafeLoader)
    db_setup(args.db_file)

    with open(os.path.join("configs", "regression-metadata.yaml")) as m_file:
        metadata = yaml.load(m_file, Loader=SafeLoader)
    status = compile_reporting_status(metadata)

    if args.load_test_data:
        with open('bisections.dump', 'rb') as f:
            bisections = pickle.load(f)
        with open('analysis.dump', 'rb') as f:
            analysis = pickle.load(f)
    else:
        emails = fetch_messages(args.messages, args.messages_load, args.messages_dump)
        bisections = collect_bisections(emails, branches['branches'])
        lore_thread = threading.Thread(target=get_lore_for_bisections, args=(bisections, ))
        lore_thread.start()
        collect_regressions(branches['branches'], days=args.days)
        lore_thread.join()
        # Store all bisections and related regressions and tests in the DB
        for b in bisections.get_all_bisections():
            b.save(db_conn())
        if args.dump_test_data:
            with open('bisections.dump', 'wb') as f:
                pickle.dump(bisections, f, protocol=pickle.HIGHEST_PROTOCOL)

        update_regression_metadata(metadata, db_conn())
        db_helpers.db_set_last_update_timestamp()

        analysis = regression_analysis(args.update_analysis)
        if args.dump_test_data:
            with open('analysis.dump', 'wb') as f:
                pickle.dump(analysis, f, protocol=pickle.HIGHEST_PROTOCOL)

    generate_html_reports(args.output, bisections, analysis, status)


def rt_add_run_tracker_cmd(subparsers):
    parser = subparsers.add_parser('run',
                help='run tracker and generate html reports')
    parser.add_argument('--branches', "-b", type=str, metavar="STRING",
                      help="YAML files with the tree and braches to track")
    parser.add_argument('--output', "-o", type=str, metavar="STRING",
                      help="output dir for the HTML reports")
    parser.add_argument('--messages', "-m", type=int, default=100,
                      help="number of email messages to fetch from archives")
    parser.add_argument('--messages-dump', type=str, metavar="STRING",
                      help="pickle dump of the downloaded messages dict and return.")
    parser.add_argument('--messages-load', type=str, metavar="STRING",
                      help="load pickle messages dict. Skip actual download phase.")
    parser.add_argument('--update-analysis', type=int, metavar="DAYS",
                      help="update regression analysis older than this number of days. 0 = update all")
    parser.add_argument('--days', type=int, metavar="DAYS", default=1,
                      help="number of days back to search for regressions (default = 1)")
    parser.add_argument('--db-file', type=str, metavar="STRING",
                        default='regtracker.db',
                        help="path to the database file to use")
    parser.add_argument('--dump-test-data', action='store_true',
                        help="dumps pickle files for test runs (see also --load-test-data)")
    parser.add_argument('--load-test-data', action='store_true',
                        help="runs in test mode: don't retrieve new data, get it all from the DB and pickle files")
    parser.set_defaults(func=run_tracker)
